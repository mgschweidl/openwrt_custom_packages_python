#
# Copyright (C) 2015, 2017-2018 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=python-py
PKG_VERSION:=1.5.3
PKG_RELEASE:=1

PKG_SOURCE:=py-$(PKG_VERSION).tar.gz
PKG_SOURCE_URL:=https://files.pythonhosted.org/packages/f7/84/b4c6e84672c4ceb94f727f3da8344037b62cee960d80e999b1cd9b832d83
PKG_HASH:=29c9fab495d7528e80ba1e343b958684f4ace687327e6f789a94bf3d1915f881

PKG_BUILD_DIR:=$(BUILD_DIR)/$(BUILD_VARIANT)-py-$(PKG_VERSION)

PKG_LICENSE:=MIT License (MIT)
PKG_MAINTAINER:=Jeffery To <jeffery.to@gmail.com>, Alexandru Ardelean <ardeleanalex@gmail.com>

include $(INCLUDE_DIR)/package.mk
include $(TOPDIR)/feeds/packages/lang/python/python-package.mk
include $(TOPDIR)/feeds/packages/lang/python/python3-package.mk

PKG_UNPACK:=$(HOST_TAR) -C $(PKG_BUILD_DIR) --strip-components=1 -xzf $(DL_DIR)/$(PKG_SOURCE)

define Package/python-py/Default
  SECTION:=lang
  CATEGORY:=Languages
  SUBMENU:=Python
  URL:=https://pypi.org/project/py/
endef

define Package/python-py
$(call Package/python-py/Default)
  TITLE:=python-py
  DEPENDS:=+PACKAGE_python-py:python-light
  VARIANT:=python
endef

define Package/python3-py
$(call Package/python-py/Default)
  TITLE:=python3-py
  DEPENDS:=+PACKAGE_python3-py:python3-light
  VARIANT:=python3
endef

define Package/python-py/description
The py lib is a Python development support library featuring the following tools and modules:
- py.path: uniform local and svn path objects
- py.apipkg: explicit API control and lazy-importing
- py.iniconfig: easy parsing of .ini files
- py.code: dynamic code generation and introspection (deprecated, moved to pytest).
endef

define Package/python3-py/description
$(call Package/python-py/description)
.
(Variant for Python3)
endef

$(eval $(call PyPackage,python-py))
$(eval $(call BuildPackage,python-py))
$(eval $(call BuildPackage,python-py-src))

$(eval $(call Py3Package,python3-py))
$(eval $(call BuildPackage,python3-py))
$(eval $(call BuildPackage,python3-py-src))
