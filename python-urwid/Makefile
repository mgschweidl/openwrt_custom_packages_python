#
# Copyright (C) 2015, 2017-2018 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=python-urwid
PKG_VERSION:=2.0.1
PKG_RELEASE:=1

PKG_SOURCE:=urwid-$(PKG_VERSION).tar.gz
PKG_SOURCE_URL:=https://files.pythonhosted.org/packages/c7/90/415728875c230fafd13d118512bde3184d810d7bf798a631abc05fac09d0
PKG_HASH:=644d3e3900867161a2fc9287a9762753d66bd194754679adb26aede559bcccbc

PKG_BUILD_DIR:=$(BUILD_DIR)/$(BUILD_VARIANT)-urwid-$(PKG_VERSION)

PKG_LICENSE:=MIT License (MIT)
PKG_MAINTAINER:=Jeffery To <jeffery.to@gmail.com>, Alexandru Ardelean <ardeleanalex@gmail.com>

include $(INCLUDE_DIR)/package.mk
include $(TOPDIR)/feeds/packages/lang/python/python-package.mk
include $(TOPDIR)/feeds/packages/lang/python/python3-package.mk

PKG_UNPACK:=$(HOST_TAR) -C $(PKG_BUILD_DIR) --strip-components=1 -xzf $(DL_DIR)/$(PKG_SOURCE)

define Package/python-urwid/Default
  SECTION:=lang
  CATEGORY:=Languages
  SUBMENU:=Python
  URL:=https://pypi.org/project/urwid/
endef

define Package/python-urwid
$(call Package/python-urwid/Default)
  TITLE:=python-urwid
  DEPENDS:=+PACKAGE_python-urwid:python-light
  VARIANT:=python
endef

define Package/python3-urwid
$(call Package/python-urwid/Default)
  TITLE:=python3-urwid
  DEPENDS:=+PACKAGE_python3-urwid:python3-light
  VARIANT:=python3
endef

define Package/python-urwid/description
Urwid is a console user interface library for Python. It inlcudes
many features useful for text console applciation developers
including:
- Applications resize quickly and smoothly
- Automatic, programmable text alignment and wrapping
- Simple markup for setting text attributes within blocks of text
- Powerful list box with programmable content for scrolling all widget types
- Your choice of event loops: Twisted, Glib, Tornado or select-based loop
- Pre-built widgets include edit boxes, buttons, check boxes and radio buttons
- Display modules include raw, curses, and experimental LCD and web displays
- Support for UTF-8, simple 8-bit and CJK encodings
- 256 and 88 color mode support
- Compatible with Python 2.6, 2.7, 3.2+ and PyPy
endef

define Package/python3-urwid/description
$(call Package/python-urwid/description)
.
(Variant for Python3)
endef

$(eval $(call PyPackage,python-urwid))
$(eval $(call BuildPackage,python-urwid))
$(eval $(call BuildPackage,python-urwid-src))

$(eval $(call Py3Package,python3-urwid))
$(eval $(call BuildPackage,python3-urwid))
$(eval $(call BuildPackage,python3-urwid-src))
