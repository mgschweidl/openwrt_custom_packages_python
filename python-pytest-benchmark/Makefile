#
# Copyright (C) 2015, 2017-2018 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=python-pytest-benchmark
PKG_VERSION:=3.1.1
PKG_RELEASE:=1

PKG_SOURCE:=pytest-benchmark-$(PKG_VERSION).tar.gz
PKG_SOURCE_URL:=https://files.pythonhosted.org/packages/e1/35/30e3e108dd3db531d3200f9e3234210a600fea5efc971e3dacd2458e2153
PKG_HASH:=185526b10b7cf1804cb0f32ac0653561ef2f233c6e50a9b3d8066a9757e36480

PKG_BUILD_DIR:=$(BUILD_DIR)/$(BUILD_VARIANT)-pytest-benchmark-$(PKG_VERSION)

PKG_LICENSE:=BSD License (BSD)
PKG_MAINTAINER:=Jeffery To <jeffery.to@gmail.com>, Alexandru Ardelean <ardeleanalex@gmail.com>

include $(INCLUDE_DIR)/package.mk
include $(TOPDIR)/feeds/packages/lang/python/python-package.mk
include $(TOPDIR)/feeds/packages/lang/python/python3-package.mk

PKG_UNPACK:=$(HOST_TAR) -C $(PKG_BUILD_DIR) --strip-components=1 -xzf $(DL_DIR)/$(PKG_SOURCE)

define Package/python-pytest-benchmark/Default
  SECTION:=lang
  CATEGORY:=Languages
  SUBMENU:=Python
  URL:=https://pypi.org/project/pytest-benchmark/
endef

define Package/python-pytest-benchmark
$(call Package/python-pytest-benchmark/Default)
  TITLE:=python-pytest-benchmark
  DEPENDS:=+PACKAGE_python-pytest-benchmark:python-light
  VARIANT:=python
endef

define Package/python3-pytest-benchmark
$(call Package/python-pytest-benchmark/Default)
  TITLE:=python3-pytest-benchmark
  DEPENDS:=+PACKAGE_python3-pytest-benchmark:python3-light
  VARIANT:=python3
endef

define Package/python-pytest-benchmark/description
A py.test fixture for benchmarking code. It will group the
tests into rounds that are calibrated to the chosen timer.
endef

define Package/python3-pytest-benchmark/description
$(call Package/python-pytest-benchmark/description)
.
(Variant for Python3)
endef

$(eval $(call PyPackage,python-pytest-benchmark))
$(eval $(call BuildPackage,python-pytest-benchmark))
$(eval $(call BuildPackage,python-pytest-benchmark-src))

$(eval $(call Py3Package,python3-pytest-benchmark))
$(eval $(call BuildPackage,python3-pytest-benchmark))
$(eval $(call BuildPackage,python3-pytest-benchmark-src))
